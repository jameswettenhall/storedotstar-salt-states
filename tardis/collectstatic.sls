curl-pkg:
  pkg:
    - installed
    - name: curl

nodejs-apt-update:
  cmd.script:
    - source: https://deb.nodesource.com/setup_10.x
    - require:
      - pkg: curl-pkg

nodejs-pkg:
  pkg:
    - installed
    - name: nodejs
    - require:
      - cmd: nodejs-apt-update

static_files_dir:
  file:
    - name: /mnt/static_files
    - directory
    - user: mytardis
    - group: mytardis
    - mode: 755

tardis_npm_install:
  cmd.run:
    - name: su mytardis -c 'npm install --production'
    - cwd: /home/mytardis/mytardis
    - user: root
    - shell: /bin/bash
    - onlyif: test -f /home/mytardis/mytardis/package.json
    - require:
      - pkg: nodejs-pkg

tardis_collect_static:
  cmd.run:
    - name: su mytardis -c '/home/mytardis/virtualenvs/mytardis/bin/python mytardis.py collectstatic -c --noinput'
    - cwd: /home/mytardis/mytardis
    - user: root
    - shell: /bin/bash
    - require:
      - cmd: tardis_npm_install
