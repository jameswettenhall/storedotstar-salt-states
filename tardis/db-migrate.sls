{% set tardis_db_user = salt['pillar.get']('tardis:db_user', 'mytardis-'+grains['deployment']) %}
{% set tardis_db_password = salt['pillar.get']('tardis:db_password', 'myt4rd15') %}
{% set tardis_db_name = salt['pillar.get']('tardis:db_name', 'mytardis-'+grains['deployment']) %}

{% if salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound') %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% else %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_server_master', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% endif %}

{% set pg_admin_user = pillar['postgresql_admin_credentials']['user'] %}
{% set pg_admin_password = pillar['postgresql_admin_credentials']['password'] %}

postgresql-client:
  pkg:
    - installed

# Run the migration on only the first web server in the deployment
{% if salt['mine.get']('G@roles:www and G@deployment:'+grains['deployment'], 'network.ipaddrs', expr_form='compound').items()[0][1][0] == salt['network.interfaces']()['eth0']['inet'][0]['address'] %}
db_user:
  postgres_user:
    - name: {{ tardis_db_user }}
    - password: {{ tardis_db_password }}
    - present
    - createdb: False
    - createroles: False
    - createuser: False
    - login: True
    - superuser: False
    - replication: False
    - db_user: {{ pg_admin_user }}
    - db_password: {{ pg_admin_password }}
    - db_host: {{ tardis_db_host }}
    - db_port: 5432
    - require:
      - pkg: postgresql-client

tardis_db:
  postgres_database:
    - name: {{ tardis_db_name }}
    - present
    - owner: {{ tardis_db_user }}
    - db_user: {{ pg_admin_user }}
    - db_password: {{ pg_admin_password }}
    - db_host: {{ tardis_db_host }}
    - db_port: 5432
    - require:
      - postgres_user: db_user

tardis_db_migrate:
    cmd.run:
      - name: count=0; until /home/mytardis/virtualenvs/mytardis/bin/python mytardis.py migrate; do ((count++)); if [ $count -gt 10 ]; then exit 1; fi; done
      - shell: /bin/bash
      - cwd: /home/mytardis/mytardis
      - user: mytardis
      - require:
        - postgres_database: tardis_db

tardis_cache_tables:
  cmd.run:
    - name: /home/mytardis/virtualenvs/mytardis/bin/python mytardis.py createcachetable
    - cwd: /home/mytardis/mytardis
    - user: mytardis
    - require:
      - postgres_database: tardis_db
      - cmd: tardis_db_migrate
{% endif %}
