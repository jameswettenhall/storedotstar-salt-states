{% set tardis_db_user = salt['pillar.get']('tardis:db_user', 'mytardis-'+grains['deployment']) %}
{% set tardis_db_password = salt['pillar.get']('tardis:db_password', 'myt4rd15') %}
{% set tardis_db_name = salt['pillar.get']('tardis:db_name', 'mytardis-'+grains['deployment']) %}

{% if salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound') %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% else %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_server_master', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% endif %}

{% if salt['mine.get']('G@roles:elasticsearch_loadbalancer', 'network.ipaddrs', expr_form='compound') %}
{% set elasticsearch_host = salt['mine.get']('G@roles:elasticsearch_loadbalancer', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% elif salt['mine.get']('G@roles:elasticsearch', 'network.ipaddrs', expr_form='compound') %}
{% set elasticsearch_host = salt['mine.get']('G@roles:elasticsearch', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% else %}
{% set elasticsearch_host = None %}
{% endif %}

{% set rabbitmq_user = pillar['rabbitmq']['user'] %}
{% set rabbitmq_password = pillar['rabbitmq']['password'] %}
{% set rabbitmq_vhost = pillar['rabbitmq']['vhost'] %}
{% set rabbitmq_ip = salt['mine.get']('G@roles:rabbitmq and G@deployment:'+grains['deployment'], 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}

tardis_settings_json:
  file.serialize:
    - name: /home/mytardis/mytardis/tardis/settings.json
    - dataset_pillar: tardis
    - formatter: json
    - user: root
    - group: mytardis

tardis_settings_py:
  file:
    - name: /home/mytardis/mytardis/tardis/settings.py
    - managed
    - source: salt://tardis/templates/settings.py
    - template: jinja
    - user: root
    - group: mytardis
    - mode: 650
    - defaults:
        debug: {{ pillar.get('tardis:settings:debug', False) }}
        tardis_db: {{ tardis_db_name }}
        tardis_db_user: {{ tardis_db_user }}
        tardis_db_password: {{ tardis_db_password }}
        tardis_db_host: {{ tardis_db_host }}
        rabbitmq_user: {{ rabbitmq_user }}
        rabbitmq_password: {{ rabbitmq_password }}
        rabbitmq_vhost: {{ rabbitmq_vhost }}
        rabbitmq_ip: {{ rabbitmq_ip }}
        elasticsearch_host: {{ elasticsearch_host }}
    - require:
        - file: tardis_settings_json
