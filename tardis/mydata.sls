include:
  - tardis.base

/home/mytardis/mytardis/tardis/apps/mydata:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
      - git: mytardis-git

mydata-app:
  git:
    - name: https://github.com/mytardis/mytardis-app-mydata.git
    - user: mytardis
    - latest
    - target: /home/mytardis/mytardis/tardis/apps/mydata
    - force_clone: true
    - force_checkout: true
    - force_reset: true
    - require:
      - file: /home/mytardis/mytardis/tardis/apps/mydata
