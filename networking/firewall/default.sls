iptables-persistent:
  pkg:
    - installed

pre_flush_policy_ipv4:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: ACCEPT    
    - family: ipv4

pre_flush_policy_ipv6:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: ACCEPT
    - family: ipv6

clean_iptables_ipv4:
  iptables.flush:
    - table: filter
    - family: ipv4
    - require:
      - iptables: pre_flush_policy_ipv4

clean_iptables_ipv6:
  iptables.flush:
    - table: filter
    - family: ipv6
    - require:
      - iptables: pre_flush_policy_ipv6

{% if 'saltmaster' in grains['roles'] %}
{% if salt['pillar.get']('networking:trusted_ips', None) %}
{% for trusted_ip_source in salt['pillar.get']('networking:trusted_ips', []) %}
ssh_ipv4_{{ trusted_ip_source }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - dport: 22
    - proto: tcp
    - family: ipv4
    - source: {{ trusted_ip_source }}
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
{% endfor %}
{% else %}
ssh_ipv4:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - dport: 22
    - proto: tcp
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
{% endif %}
{% endif %}

local_interface_ipv4:
  iptables.append:
    - table: filter
    - chain: INPUT
    - in-interface: lo
    - jump: ACCEPT
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

local_interface_ipv6:
  iptables.append:
    - table: filter
    - chain: INPUT
    - in-interface: lo
    - jump: ACCEPT
    - family: ipv6
    - save: True
    - require:
      - iptables: clean_iptables_ipv6

established_connections_ipv4:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: ESTABLISHED,RELATED
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

established_connections_ipv6:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: ESTABLISHED,RELATED
    - family: ipv6
    - save: True
    - require:  
      - iptables: clean_iptables_ipv6

{% for host, data in salt['mine.get']('*', 'network.ipaddrs').items() -%}
{% for address in data %}
allow_{{ host }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW,ESTABLISHED,RELATED
    - family: ipv4
    - source: {{ address }}
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
    - require_in:
      - iptables: default_input_policy_drop_ipv4
      - iptables: default_output_policy_accept_ipv4
      - iptables: default_forward_policy_drop_ipv4
{% endfor %}
{% endfor %}

default_input_policy_drop_ipv4:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: DROP
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

default_output_policy_accept_ipv4:
  iptables.set_policy:
    - table: filter
    - chain: OUTPUT
    - policy: ACCEPT
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

default_forward_policy_drop_ipv4:
  iptables.set_policy:
    - table: filter
    - chain: FORWARD
    - policy: DROP
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

default_input_policy_drop_ipv6:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: DROP
    - family: ipv6
    - save: True
    - require:
      - iptables: clean_iptables_ipv6

default_output_policy_accept_ipv6:
  iptables.set_policy:
    - table: filter
    - chain: OUTPUT
    - policy: ACCEPT
    - family: ipv6
    - save: True
    - require:
      - iptables: clean_iptables_ipv6

default_forward_policy_drop_ipv6:
  iptables.set_policy:
    - table: filter
    - chain: FORWARD
    - policy: DROP
    - family: ipv6
    - save: True
    - require:
      - iptables: clean_iptables_ipv6

{% if 'saltmaster' in grains['roles'] %}
include:
  - networking.firewall.salt
{% endif %}

{% if 'haproxy' in grains['roles'] %}
include:
  - networking.firewall.web
  - networking.firewall.sftpd
  - networking.firewall.mydata_scp
  - networking.firewall.rabbitmq_management
{% endif %}

{% if not salt['mine.get']('G@roles:haproxy', 'network.ipaddrs', expr_form='compound').items() %}
{% if 'www' in grains['roles'] %}
include:
  - networking.firewall.web
{% endif %}
{% endif %}

{% for trusted_ip_source in salt['pillar.get']('networking:trusted_monitoring_ips', []) %}
{% if 'www' in grains['roles'] %}
https_monitoring_{{ trusted_ip_source }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - dport: 443
    - proto: tcp
    - family: ipv4
    - source: {{ trusted_ip_source }}
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
{% endif %}
{% endfor %}

{% if not salt['mine.get']('G@roles:haproxy', 'network.ipaddrs', expr_form='compound').items() %}
{% if 'sftpd' in grains['roles'] %}
include:
  - networking.firewall.sftpd
{% endif %}
{% endif %}
