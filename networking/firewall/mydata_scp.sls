{% if salt['pillar.get']('networking:trusted_ips', None) %}
{% for trusted_ip_source in salt['pillar.get']('networking:trusted_ips', []) %}
mydata_scp_{{ trusted_ip_source }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 2201
    - proto: tcp
    - family: ipv4
    - source: {{ trusted_ip_source }}
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
{% endfor %}
{% else %}
mydata_scp:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 2201
    - proto: tcp
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
{% endif %}
