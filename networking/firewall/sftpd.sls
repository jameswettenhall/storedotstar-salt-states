sftpd:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 2200
    - proto: tcp
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
