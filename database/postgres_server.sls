# Installs postgresql in streaming replication mode

include:
  - database.postgres_client
  - database.postgres_keys
  - storage

postgresql:
  pkg.installed:
  - name: postgresql-9.3
  - require:
    - pkgrepo: repository_pgdg
  service.running:
    - require:
      - cmd: initial_db
    - watch:
      - file: /etc/postgresql/9.3/main/postgresql.conf
      - file: /etc/postgresql/9.3/main/pg_hba.conf
{% if 'postgresql_server_slave' in grains['roles'] %}
      - file: /pg_data/9.3/main/recovery.conf
{% endif %}

postgresql-contrib:
  pkg:
    - installed

postgresql-9.3-pgpool2:
  pkg:
    - installed

# Set up the data directory
/var/lib/tardis_storage/pg_data:
  file.directory:
    - user: postgres
    - group: root
    - mode: 770
    - makedirs: True
    - require:
        - sls: storage
        - pkg: postgresql
          
/pg_data:
  file.symlink:
    - target: /var/lib/tardis_storage/pg_data
    - require:
        - file: /var/lib/tardis_storage/pg_data
          
# Create the directory for the new database
/pg_data/9.3/main:
  file.directory:
    - user: postgres
    - group: postgres
    - dir_mode: 700
    - makedirs: True
    - require:
        - file: /pg_data

# Initalises the database either with a fresh db, or one backed up from the master.
initial_db:
  cmd.run:
{% if 'postgresql_server_master' in grains['roles'] %}
    - name: pkill postgres; /usr/lib/postgresql/9.3/bin/initdb -D /pg_data/9.3/main && rm /pg_data/9.3/main/*.conf
{% else %}
    - name: pkill postgres; /usr/bin/pg_basebackup -h {{ salt['mine.get']('G@deployment:'+grains['deployment']+' and G@roles:postgresql_server_master', 'grains.items', expr_form='compound').items()[0][0] }} -D /pg_data/9.3/main
    - shell: /bin/bash
{% endif %}
    - user: postgres
    - creates: /pg_data/9.3/main/PG_VERSION
    - require:
        - file: /pg_data/9.3/main
        - file: /etc/postgresql/9.3/main/pg_hba.conf
        - sls: database.postgres_keys

/etc/postgresql/9.3/main/postgresql.conf:
  file:
    - managed
    - source: salt://database/templates/postgresql.conf
    - template: jinja
    - mode: 644
    - user: postgres
    - group: postgres
    - require:
        - pkg: postgresql
        - cmd: initial_db

/var/lib/postgresql/9.3/main:
  file:
    - absent
    - require:
        - pkg: postgresql

/pg_data/9.3/main/pgpool_remote_start:
  file:
    - managed
    - source: salt://database/templates/pgpool_remote_start
    - mode: 755
    - user: postgres
    - group: postgres
    - require:
        - cmd: initial_db

/pg_data/9.3/main/basebackup.sh:
  file:
    - managed
    - source: salt://database/templates/basebackup.sh
    - mode: 755
    - user: postgres
    - group: postgres
    - require:
        - cmd: initial_db

/etc/sudoers.d/postgresql:
  file:
    - managed
    - source: salt://database/templates/postgres_sudoers
    - mode: 440
    - user: root
    - group: root

/var/lib/postgresql/set_role_master:
  file:
    - managed
    - source: salt://database/templates/set_role_master
    - mode: 755
    - user: root
    - group: root
    - require:
        - pkg: postgresql

/var/lib/postgresql/set_role_slave:
  file:
    - managed
    - source: salt://database/templates/set_role_slave
    - mode: 755
    - user: root
    - group: root
    - require:
        - pkg: postgresql

/etc/postgresql/9.3/main/pg_hba.conf:
  file:
    - managed
    - source: salt://database/templates/pg_hba.conf
    - mode: 640
    - user: postgres
    - group: postgres
    - template: jinja
    - defaults:
        postgres_user: {{ salt['pillar.get']('postgresql_admin_credentials:user', 'postgres') }}
    - require:
        - pkg: postgresql

{% if 'postgresql_server_master' in grains['roles'] %}
postgres_superuser:
  postgres_user:
    - name: {{ salt['pillar.get']('postgresql_admin_credentials:user', 'postgres') }}
    - present
    - user: postgres
    - createdb: True
    - createroles: True
    - createuser: True
    - encrypted: True
    - login: True
    - superuser: True
    - replication: True
    - password: {{ salt['pillar.get']('postgresql_admin_credentials:password', '') }}
    - refresh_password: True
    - require:
        - service: postgresql
        - file: /etc/postgresql/9.3/main/postgresql.conf
        - cmd: initial_db

postgres_functions:
  cmd.run:
    - name: psql -f /usr/share/postgresql/9.3/extension/pgpool-recovery.sql template1 && psql -f /usr/share/postgresql/9.3/extension/pgpool-recovery.sql postgres && psql -f /usr/share/postgresql/9.3/extension/pgpool-regclass.sql template1 && psql -f /usr/share/postgresql/9.3/extension/pgpool-regclass.sql postgres && touch /pg_data/9.3/main/pgpool_function_setup.done
    - user: postgres
    - creates: /pg_data/9.3/main/pgpool_function_setup.done
    - require:
        - service: postgresql
        - file: /etc/postgresql/9.3/main/postgresql.conf
        - cmd: initial_db
{% endif %}

{% if 'postgresql_server_slave' in grains['roles'] %}
/pg_data/9.3/main/recovery.conf:
  file.managed:
    - source: salt://database/templates/recovery.conf
    - template: jinja
    - user: postgres
    - group: postgres
    - require:
        - cmd: initial_db
{% endif %}

# This helper script can be used to monitor IO performance

/var/lib/postgresql/iowait_check.sh:
  file.managed:
    - source: salt://database/templates/iowait_check.sh
    - user: root
    - group: root
    - mode: 755
    - require:
        - pkg: sysstat
